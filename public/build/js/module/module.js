/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/assets/js/module/module.js":
/*!**********************************************!*\
  !*** ./resources/assets/js/module/module.js ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  $('#modules_table').DataTable({
    autoWidth: true,
    processing: true,
    serverSide: true,
    ordering: false,
    ajax: {
      url: app_url + 'admin/module/get-list',
      type: 'post'
    },
    searching: true,
    columns: [{
      data: 'DT_RowIndex',
      className: 'tx-center',
      searchable: false
    }, {
      data: 'display_name'
    }, {
      data: 'module_category_name',
      searchable: false,
      className: 'tx-center'
    }, {
      data: 'note'
    }, {
      data: 'status',
      className: 'tx-center'
    }, {
      data: 'created_at',
      className: 'tx-center'
    }, {
      data: 'action',
      className: 'tx-center'
    }]
  });
  $('#modules_table').on('click', '.btn-warning', function () {
    window.location.href = app_url + 'admin/module/' + $(this).data('id') + '/edit';
  });
  $('#modules_table').on('click', '.btn-danger', function (event) {
    var _this = this;

    event.preventDefault();
    swal({
      title: Lang.get('global.are_you_sure_to_delete'),
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#00b297',
      cancelButtonColor: '#d33',
      confirmButtonText: Lang.get('global.confirm'),
      cancelButtonText: Lang.get('global.cancle')
    }).then(function (result) {
      if (result.value) {
        $.ajax({
          url: app_url + 'admin/module/' + $(_this).data('id'),
          type: 'DELETE',
          dataType: "JSON",
          data: {
            id: $(_this).data('id')
          },
          success: function success(res) {
            if (!res.err) {
              toastr.error(Lang.get('global.delete_success'));
              setTimeout(function () {
                window.location.reload();
              }, 2000);
            }
          }
        });
      }
    });
  });
});

/***/ }),

/***/ 3:
/*!****************************************************!*\
  !*** multi ./resources/assets/js/module/module.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\xampp\htdocs\chidung\resources\assets\js\module\module.js */"./resources/assets/js/module/module.js");


/***/ })

/******/ });